/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Absolute Orientation problem using quaternions.

  File          : quatAbsOrientationMx.cpp
  Date          : 07/05/2000 - 19/06/2009

  Compiler      : g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2009 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <cmath>            // STL
#include <iostream>
#include <mex.h>            // Matlab Mex Functions

#include <gsl/gsl_math.h>   // GNU Scientific Library
#include <gsl/gsl_eigen.h>
        
#include <quaternions.h>

// Define for strict checking of the input/output parameters (slower)
// #define DO_STRICT_CHECK
// To ouptut Debug Information
// #define DO_DEBUG

#define ERROR_HEADER  "MATLAB:quatAbsOrientationMx:Input\n"

// Identify the input parameters by it's index
enum { LP = 0, RP, SCALETYPE };
// Identify the output parameters by it's index
enum { R = 0, T, S };

// Mex function
//
void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter count checks
#ifdef DO_STRICT_CHECK
  if ( ( nrhs != 2 && nrhs != 3 ) || ( nlhs != 2 && nrhs != 3 ) )
#else
  if ( nrhs < 2 )
#endif
    mexErrMsgTxt ( ERROR_HEADER "Usage: [R, t [, s]] = quatAbsOrientationMx.cpp ( Lp, Rp [, ScaleType = 1] )" );

  int NumRows = mxGetM ( prhs[LP] );
  int NumPoints = mxGetN ( prhs[LP] );

  if ( mxGetNumberOfElements ( prhs[SCALETYPE] ) != 1 || !mxIsDouble ( prhs[SCALETYPE] ) )
    mexErrMsgTxt ( ERROR_HEADER "ScaleType must be a double scalar!" );

  if ( mxGetN ( prhs[RP] ) != NumPoints || mxGetM ( prhs[RP] ) != NumRows || NumRows != 3 || !mxIsDouble ( prhs[LP] ) || !mxIsDouble ( prhs[RP] ) )
    mexErrMsgTxt ( ERROR_HEADER "Lp and Rp must be double vectors of 3D points with the same number of elements (3xn)!" );

  int ScaleType;

  if ( nrhs < 3 )
    ScaleType = 1;
  else
    ScaleType = static_cast<int> ( *(double *)mxGetPr ( prhs[SCALETYPE] ) );

  // Compute Mean
  double rlx, rly, rlz;
  double rrx, rry, rrz;
  int i;
  
  rlx = rly = rlz = 0.0;
  rrx = rry = rrz = 0.0;

  double *Lp = (double *)mxGetPr ( prhs[LP] );
  double *Rp = (double *)mxGetPr ( prhs[RP] );
  for ( i = 0; i < NumPoints; i++ )
  {
    rlx += *Lp++; rly += *Lp++; rlz += *Lp++;
    rrx += *Rp++; rry += *Rp++; rrz += *Rp++;
  }

  // Average
  rlx /= static_cast<double> ( NumPoints );
  rly /= static_cast<double> ( NumPoints );
  rlz /= static_cast<double> ( NumPoints );

  rrx /= static_cast<double> ( NumPoints );
  rry /= static_cast<double> ( NumPoints );
  rrz /= static_cast<double> ( NumPoints );

#ifdef DO_DEBUG
  printf ( "rl=(%f, %f, %f)\n", rlx, rly, rlz );
  printf ( "rr=(%f, %f, %f)\n", rrx, rry, rrz );
#endif

  // Compute M
  //
  //      | Sxx Sxy Sxz |
  //  M = | Syx Syy Syz | = ( Lp - [llx;lly;llz] ) * ( Rp' - [rlx rly rlz] )
  //      | Szx Szy Szz |
  //
  double Sxx, Sxy, Sxz, Syx, Syy, Syz, Szx, Szy, Szz;
  double lx, ly, lz, rx, ry, rz;

  Sxx = Sxy = Sxz = Syx = Syy = Syz = Szx = Szy = Szz = 0.0;

  Lp = (double *)mxGetPr ( prhs[LP] );
  Rp = (double *)mxGetPr ( prhs[RP] );
  for ( i = 0; i < NumPoints; i++ )
  {
    lx = *Lp++; ly = *Lp++; lz = *Lp++;
    rx = *Rp++; ry = *Rp++; rz = *Rp++;

    Sxx += (lx - rlx) * (rx - rrx);
    Sxy += (lx - rlx) * (ry - rry);
    Sxz += (lx - rlx) * (rz - rrz);
    Syx += (ly - rly) * (rx - rrx);
    Syy += (ly - rly) * (ry - rry);
    Syz += (ly - rly) * (rz - rrz);
    Szx += (lz - rlz) * (rx - rrx);
    Szy += (lz - rlz) * (ry - rry);
    Szz += (lz - rlz) * (rz - rrz);
  }
  
#ifdef DO_DEBUG
  printf ( "    |%f  %f  %f|\n", Sxx, Sxy, Sxz );
  printf ( "M = |%f  %f  %f|\n", Syx, Syy, Syz );
  printf ( "    |%f  %f  %f|\n", Szx, Szy, Szz );
#endif

  // Compute N
  //
  //       | Sxx+Syy+Szz    Syz_zy       Szx_xz       Sxy_yx    |
  //       |   Syz_zy     Sxx-Syy-Szz    Sxyyx        Szxxz     |
  //   N = |   Szx_xz        Sxyyx    -Sxx+Syy-Szz     Syzzy    |
  //       |   Sxy_yx        Szxxz        Syzzy    -Sxx-Syy+Szz |
  //
  double Syz_zy = Syz - Szy;
  double Szx_xz = Szx - Sxz;
  double Sxy_yx = Sxy - Syx;
  double Sxyyx  = Sxy + Syx;
  double Szxxz  = Szx + Sxz;
  double Syzzy  = Syz + Szy;

  double N[4*4];
  N[ 0] = Sxx+Syy+Szz; N[ 1] = Syz_zy;      N[ 2] = Szx_xz;       N[ 3] = Sxy_yx;
  N[ 4] = Syz_zy;      N[ 5] = Sxx-Syy-Szz; N[ 6] = Sxyyx;        N[ 7] = Szxxz;
  N[ 8] = Szx_xz;      N[ 9] = Sxyyx;       N[10] = -Sxx+Syy-Szz; N[11] = Syzzy;
  N[12] = Sxy_yx;      N[13] = Szxxz;       N[14] = Syzzy;        N[15] = -Sxx-Syy+Szz;

#ifdef DO_DEBUG
  printf ( "    |%f  %f  %f  %f|\n", N[ 0], N[ 1], N[ 2], N[ 3] );
  printf ( "    |%f  %f  %f  %f|\n", N[ 4], N[ 5], N[ 6], N[ 7] );
  printf ( "N = |%f  %f  %f  %f|\n", N[ 8], N[ 9], N[10], N[11] );
  printf ( "    |%f  %f  %f  %f|\n", N[12], N[13], N[14], N[15] );
#endif

  // Eigen Analisys of N
  gsl_matrix_view m = gsl_matrix_view_array ( N, 4, 4 );
  gsl_vector *eval = gsl_vector_alloc ( 4 );
  gsl_matrix *evec = gsl_matrix_alloc ( 4, 4 );
  gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc ( 4 );
  gsl_eigen_symmv ( &m.matrix, eval, evec, w );
  gsl_eigen_symmv_free ( w );
  // Sort Eigen Values
  gsl_eigen_symmv_sort ( eval, evec, GSL_EIGEN_SORT_VAL_DESC );

  // Take que biggest
  double D = gsl_vector_get ( eval, 0 );

#ifdef DO_DEBUG
  printf ( " EigenValues = (%f  %f  %f  %f)\n",
           gsl_vector_get ( eval, 0 ), gsl_vector_get ( eval, 1 ),
           gsl_vector_get ( eval, 2 ), gsl_vector_get ( eval, 3 ) );
#endif      

  gsl_vector_view v = gsl_matrix_column ( evec, 0 );

  // Vector associated to the biggest eigen value => unitary quaternion
  double q[4];
  q[0] = gsl_vector_get ( &v.vector, 0 );
  q[1] = gsl_vector_get ( &v.vector, 1 );
  q[2] = gsl_vector_get ( &v.vector, 2 );
  q[3] = gsl_vector_get ( &v.vector, 3 );

#ifdef DO_DEBUG
  printf ( " Largest EigenValue = %f\n", D );
  printf ( " Associated EigenVector q = (%f  %f  %f  %f)\n", q[0], q[1], q[2], q[3] );
#endif      

  gsl_vector_free ( eval );
  gsl_matrix_free ( evec );

  // Check that it is positive real
  if ( gsl_finite ( D ) && D > 0.0  )
  {
    plhs[R] = mxCreateDoubleMatrix ( 3, 3, mxREAL );
    double *r = (double *)mxGetPr ( plhs[R] );

    plhs[T] = mxCreateDoubleMatrix ( 3, 1, mxREAL );
    double *t = (double *)mxGetPr ( plhs[T] );

    // Translation = difference between centroid of right measurement and the scaled and
    //               rotated centroid of the left measurement
    quat2rotmat ( q, r );

    double s;
 
    if ( ScaleType == 1 )
    {
      // Compute Scale (Symmetric Scale)
      double Sx, Sy, Sz;
      double Sl, Sr;

      Sl = Sr = 0.0;

      Lp = (double *)mxGetPr ( prhs[LP] );
      Rp = (double *)mxGetPr ( prhs[RP] );
      for ( i = 0; i < NumPoints; i++ )
      {
        Sx = *Lp++; Sy = *Lp++; Sz = *Lp++;
        Sl += (Sx - rlx) * (Sx - rlx) + (Sy - rly) * (Sy - rly) + (Sz - rlz) * (Sz - rlz);

        Sx = *Rp++; Sy = *Rp++; Sz = *Rp++;
        Sr += (Sx - rrx) * (Sx - rrx) + (Sy - rry) * (Sy - rry) + (Sz - rrz) * (Sz - rrz);
      }

      s = sqrt ( Sr / Sl );
#ifdef DO_DEBUG
        printf ( "Symetric Scale s = %f\n", s );
#endif      
    }
    else
      if ( ScaleType == 2 )
      {
        // Compute Scale (Non-Symmetric Scale)
        double Sx, Sy, Sz;
        double Sl, Sr;

        Sl = Sr = 0.0;

        Lp = (double *)mxGetPr ( prhs[LP] );
        Rp = (double *)mxGetPr ( prhs[RP] );
        for ( i = 0; i < NumPoints; i++ )
        {
          Sx = *Rp++; Sy = *Rp++; Sz = *Rp++;
          Sr += (Sx - rrx) * (Sx - rrx) + (Sy - rry) * (Sy - rry) + (Sz - rrz) * (Sz - rrz);

          Sl += (Sx - rrx) * (r[0] * (Sx - rrx) + r[3] * (Sy - rry) + r[6] * (Sz - rrz) ) +
                (Sy - rry) * (r[1] * (Sx - rrx) + r[4] * (Sy - rry) + r[7] * (Sz - rrz) ) +
                (Sz - rrz) * (r[2] * (Sx - rrx) + r[5] * (Sy - rry) + r[8] * (Sz - rrz) );
        }
 
        s = Sl / Sr;

#ifdef DO_DEBUG
        printf ( "Non-Symetric Scale s = %f\n", s );
#endif      
      }
      else
        if ( ScaleType == 0 )
        {
          // No Scaling Factor
          s = 1.0;

#ifdef DO_DEBUG
          printf ( "Fixed Scale s = %f\n", s );
#endif   
        }
        else
           mexErrMsgTxt ( ERROR_HEADER 
                          "ScaleType must be either 0 (No Scale), 1 (Symmetric Scale) or 2 (Non-Symmetric Scale)!" );

    // Translation = difference between centroid of right measurement and the scaled and
    //               rotated centroid of the left measurement
    *t++ = rrx - s * ( r[0] * rlx + r[3] * rly + r[6] * rlz );
    *t++ = rry - s * ( r[1] * rlx + r[4] * rly + r[7] * rlz );
    *t   = rrz - s * ( r[2] * rlx + r[5] * rly + r[8] * rlz );

    // Return scaling factor as well
    if ( nrhs > 3 )
    {
      plhs[S] = mxCreateDoubleMatrix ( 1, 1, mxREAL );
      double *sc = (double *)mxGetPr ( plhs[S] ); 

      *sc = s;
    }

#ifdef DO_DEBUG
    t = (double *)mxGetPr ( plhs[T] );
    printf ( "t = (%f  %f  %f)\n", t[0], t[1], t[2] );
    printf ( "s = %f\n", s );
#endif   
  }
  else
  {
    // Error => q corresponds to the vector associated to the biggest positive eigen value
    std::cerr << "Eigenvalue = " << D << std::endl;
    mexWarnMsgTxt ( ERROR_HEADER "M is not rank 3, points are collinear!" );

    plhs[R] = mxCreateDoubleMatrix ( 0, 3, mxREAL );
    plhs[T] = mxCreateDoubleMatrix ( 0, 1, mxREAL );
    plhs[S] = mxCreateDoubleMatrix ( 0, 0, mxREAL );
  }
}
