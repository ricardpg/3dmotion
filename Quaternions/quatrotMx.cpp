/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Rotate a 3D point by a quaternion encoded in a 3-Vector.

  File          : quatrotMx.cpp
  Date          : 05/03/2009 - 05/03/2009

  Compiler      : g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2009 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <mex.h>            // Matlab Mex Functions

#include <quaternions.h>

// Define for strict checking of the input/output parameters (slower)
// #define DO_STRICT_CHECK

#define ERROR_HEADER  "MATLAB:quatrotMx:Input\n"
        
// Identify the input parameters by it's index
enum { V = 0, P };
// Identify the output parameters by it's index
enum { Q = 0 };

// Mex function
//
void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter count checks
#ifdef DO_STRICT_CHECK
  if ( nlhs != 1 || nrhs != 2 )
#else
  if ( nrhs != 2 )
#endif
    mexErrMsgTxt ( ERROR_HEADER "Usage: q = quat2vecMx ( v, p )" );

#ifdef DO_STRICT_CHECK
  // Get input lenghts
  int rv = mxGetM ( prhs[V] );
  int cv = mxGetN ( prhs[V] );  
  int rp = mxGetM ( prhs[P] );
  int cp = mxGetN ( prhs[P] );  

  if ( ( rv != 1 || cv != 3 ) && ( rv != 3 || cv != 1 ) &&
       ( rp != 1 || cp != 3 ) && ( rp != 3 || cp != 1 ) )
    mexErrMsgTxt ( ERROR_HEADER "Input vectors must be 1x3 or 3x1 (V encodes a Quaternion in a 3-Vector and P encodes a 3D Point)!" );
#endif

  // Create Output Quaternion
  plhs[Q] = mxCreateDoubleMatrix ( 4, 1, mxREAL );

  // Get the pointers
  double *v = (double *)mxGetPr ( prhs[V] );
  double *p = (double *)mxGetPr ( prhs[P] );
  double *q = (double *)mxGetPr ( plhs[Q] );

  // Compute
  quatrot ( v, p, q );
}
