%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : Delaunay Triangulation Functions
%  Homepage      : http://uvl.udg.edu
%
%  Module        : Test Function for Absolute Orientation Algorithm.
%
%  File          : TestAbsOr.m
%  Date          : 08/05/2009 - 08/05/2009
%  Encoding      : ISO-8859-1 (Latin-1)
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         :
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

DoTestAbsoluteOrientation = true;      % Test the mex implementation of Absolute Orientation
  DoGenerateData = true;
  DoTestMatlab = true;
  DoTestMx = true;
  DoShowResults = true;
  NumPoints = 4000;
  MaxIt = 3000;
  ErrEps = 10^-10;
  ScalingType = 2;                     % How to compute scale: -1: Random Type; 0: Fixed (=1); 1 Symetric; 2 Non-Symetric
  LRange = [100; 20; 50];              % Left Point Range
  LOffs = [-1000; 200; 100];           % Left Point Offset
  RRange = [20; 11; 122];              % Right Point Range
  ROffs = [111; 222; 3121];            % Right Point Offset


if DoGenerateData;
  fprintf ( 'Generating Left and Right %d x %d point sets...\n', NumPoints, MaxIt );
  Lp = cell ( 1, MaxIt );
  Rp = cell ( 1, MaxIt );
  for i = 1 : MaxIt;
    Lp{i} = rand ( 3, NumPoints );
    Lp{i} = ( Lp{i} .* repmat ( LRange, 1, size ( Lp{i}, 2 ) ) ) + repmat ( LOffs, 1, size ( Lp{i}, 2 ) );

    Rp{i} = rand ( 3, NumPoints ); 
    Rp{i} = ( Rp{i} .* repmat ( RRange, 1, size ( Rp{i}, 2 ) ) ) + repmat ( ROffs, 1, size ( Rp{i}, 2 ) );
  end

  if ScalingType == -1;
    St = randi ( 3, 1, MaxIt ) - 1;
  else
    St = repmat ( ScalingType, 1, MaxIt );
  end
end

if DoTestMatlab;
  fprintf ( 'Testing (%d Iterations) Matlab''s Absolute Orientation Implementation...\n', MaxIt );
  InitTime = cputime;
  R = zeros ( 3, 3, MaxIt );
  t = zeros ( 3, MaxIt );
  for i = 1 : MaxIt;
    [R(:,:,i), t(:,i)] = quatAbsOrientation ( Lp{i}, Rp{i}, St(i) );
  end
  EndTime = cputime;
  Time = EndTime - InitTime;
  fprintf ( '  Time %fs (%fh)\n', Time, Time / 3600 );
end

if DoTestMx;
  fprintf ( 'Testing (%d Iterations) C++ Mex Absolute Orientation Implementation...\n', MaxIt );
  InitTime = cputime;
  RMx = zeros ( 3, 3, MaxIt );
  tMx = zeros ( 3, MaxIt );
  for i = 1 : MaxIt;
    [RMx(:,:,i), tMx(:,i)] = quatAbsOrientationMx ( Lp{i}, Rp{i}, St(i) );
  end
  EndTime = cputime;
  Time = EndTime - InitTime;
  fprintf ( '  Time %fs (%fh)\n', Time, Time / 3600 );
end

if DoShowResults;
  fprintf ( 'Comparing results (Max allowed Error %e)...\n', ErrEps );
  ErrR = zeros ( 1, MaxIt );
  Errt = zeros ( 1, MaxIt );
  for i = 1 : MaxIt;
    ErrR(i) = norm ( R(:,:,i) - RMx(:,:,i), 'fro' );
    Errt(i) = norm ( t(:,i) - tMx(:,i) );
  end

  % Compute Number of Errors
  ErrRi = ErrR > ErrEps;
  Errti = Errt > ErrEps;
  Err = ErrRi | Errti;
  NumErrors = sum ( Err ~= 0 );

  % Compute Error Range
  MaxErrR = max ( ErrR );
  MinErrR = min ( ErrR );
  MaxErrt = max ( Errt );
  MinErrt = min ( Errt );

  % Plot Results
  if NumErrors > 0;
    fprintf ( '  %d Errors found in %d tests (Eps = %e)!\n', NumErrors, MaxIt, ErrEps );
  else
    fprintf ( '  No Errors found in %d tests!\n', MaxIt );
  end

  fprintf ( '  Error range R: (%e, %e); t: (%e, %e)!\n', MinErrR, MaxErrR, MinErrt, MaxErrt );
end
