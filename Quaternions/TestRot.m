%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Test Rotation of points using Quaternions encoded in 3-Vectors.
%
%  File          : TestRot.m
%  Date          : 05/03/2009 - 05/03/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Number of Tests
n = 1000;
% Number of Points
m = 1000;
% Scale for the 3D random points
Sx = 100;
Sy = 30;
Sz = 400;

% Epsilon to check errors
Epsilon = 1e-12;


fprintf ( 'Allocating storage for testing from random Normalized Quaternion -> 3-Vector...\n' );

% Generate a random Quaternion
qs = rand ( 4, n );
% Allocate space for 3-Vectors
vs = zeros ( 3, n );
% Allocate space for Rotation Matrices
R = zeros ( 3, 3, n );

fprintf ( 'Creating %d random Normalized Quaternions and their correspondent 3-Vectors and Rotation Matrices...\n', n );
for i = 1 : n;
  % Normalize current quaternion
  q0 = qs(:,i) / norm ( qs(:,i) );

  % Convert to 3-Vector
  vs(:,i) = quat2vecMx ( q0 );

  % Convert to Rotation Matrix
  R(:,:,i) = quat2rotmatMx ( q0 );
end


fprintf ( 'Rotate %d Random Points using (%d) Rotation Matrices and Quaternions...\n', m, n );
Pr = zeros ( 3, n, m );
Pv = zeros ( 4, n, m );
P = zeros ( 3, m );
P(1,:) = rand ( 1, m ) * Sx;
P(2,:) = rand ( 1, m ) * Sy;
P(3,:) = rand ( 1, m ) * Sz;
tic
for i = 1 : n;
  Rc = R(:,:,i);
  vc = vs(:,i);
  for j = 1 : m;
    Pr(:,i,j) = Rc * P(:,j);
    Pv(:,i,j) = quatrotMx ( vc, P(:,j) );
  end
end
toc

Pvd = zeros ( 3, n, m );
fprintf ( 'Checking zeros in the first row of quaternion-rotated points...\n' );
FirstTime = true;
r = 0;
for i = 1:n;
  for j = 1:n;
    if abs ( Pv(1,i,j) ) > Epsilon;
      if FirstTime;
        fprintf ( '  Error found in:' );
        FirstTime = false;
      end
      r = r + 1;
      fprintf ( '    (%d, %d) = %e\n', i, j, Pv(1,i,j) );
    end
    Pvd(:,i,j) = Pv(2:4,i,j);
  end
end
Zs = abs ( Pv(1,:,:) );
Zs = Zs(:);
MaxError = max ( Zs );
MinError = min ( Zs );
if r > 0; 
  fprintf ( '  %d elements out of error bound (eps = %e; Min Error = %e; Max Error = %e)!\n', r, Epsilon, MinError, MaxError );
else
  fprintf ( '  All values are in bounds (eps = %e; Min Error = %e; Max Error = %e)!\n', Epsilon, MinError, MaxError );
end


fprintf ( 'Computing diferences between types of rotations...\n' );
Ns = zeros ( n, m );
for i = 1:n;
  for j = 1:n;
    Ns(i,j) = norm ( Pr(:,i,j) - Pvd(:,i,j) );
  end
end


I = find ( Ns > Epsilon );
r = numel ( I );
if r > 0;
  MinErr = min ( abs ( Ns ( I ) ) );
  MaxErr = max ( abs ( Ns ( I ) ) );
  fprintf ( '  %d elements out of error bounds (eps = %e; Min Error = %e; Max Error = %e)!\n', r, Epsilon, MinErr, MaxErr );
else
  disp ( '  The error of ALL convertions is in bounds!' );
end
