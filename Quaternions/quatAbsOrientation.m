%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Absolute Orientation problem using quaternions.
%
%  File          : quatAbsOrientation.m
%  Date          : 02/10/2007 - 07/09/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  quatAbsOrientation Calculate the Rotation R and translation t from two
%                     sets of points viewed from two different coordinate
%                     systems (left and right). See Horn '86 paper.
%
%                            min[R,t] ( Rp - ( R * Lp + t ) )
%
%      [R, t [, s]] = quatAbsOrientation ( Lp, Rp [, ScaleType ] )
%
%     Input Parameters:
%      Lp: Set of 3xN 3D points belonging to the left coordinate system.
%      Rp: Set of 3xN 3D points belonging to the right coordinate system.
%      ScaleType: 0: To don't compute the scale.
%                 1: To compute symmetric scale calculation. To use when
%                    both sets of measuremnts (default).
%                 2: To compute non-symmetric scale calculation.
%
%                 Non-symmetric scale is used when it is known that one set
%                 of measurements is much more precise than the other. If
%                 both sets have the same uncertanty, symmetric scale
%                 should be used. In the case that it is known that there
%                 is no scale change is better use 0 (scale = 1).
%
%     Output Parameters:
%      R: 3x3 rotation matrix.
%      t: 3x1 translation vector.
%      s: 1x1 scaling factor.
%

function [R, t, s] = quatAbsOrientation ( Lp, Rp, ScaleType )
  % Test the input parameters
  error ( nargchk ( 2, 3, nargin ) );
  error ( nargoutchk ( 2, 3, nargout ) );

  % By default compute symmetric scale
  if nargin < 3, ScaleType = 1; end

  % Number of elements
  N = size ( Lp, 2 );

  if size ( Lp, 1 ) ~= 3 || size ( Rp, 1 ) ~= 3 || N ~= size ( Rp, 2 ),
    error ( 'MATLAB:quatAbsOrientation:Input', ...
            'Lp, Rp must be 3xn matrices containing 3D points in columns!' );
  end

  if N < 2,
    error ( 'MATLAB:quatAbsOrientation:Input', ...
            'To compute R, t at least 3 point correspondences are needed!' );
  end

  % Compute Centroids
  rl = mean ( Lp, 2 );
  rr = mean ( Rp, 2 );

  % Subtract centroids to the data
  l = Lp - repmat ( rl, 1, N );
  r = Rp - repmat ( rr, 1, N );

  % Compute M
  M = l * r';
  Sxx = M(1,1); Sxy = M(1,2); Sxz = M(1,3);
  Syx = M(2,1); Syy = M(2,2); Syz = M(2,3);
  Szx = M(3,1); Szy = M(3,2); Szz = M(3,3);

  % Compute N
  Syz_zy = Syz - Szy;
  Szx_xz = Szx - Sxz;
  Sxy_yx = Sxy - Syx;
  Sxyyx  = Sxy + Syx;
  Szxxz  = Szx + Sxz;
  Syzzy  = Syz + Szy;
  N = [ Sxx+Syy+Szz,   Syz_zy,       Szx_xz,      Sxy_yx; ...
          Syz_zy,    Sxx-Syy-Szz,     Sxyyx,       Szxxz; ...
          Szx_xz,       Sxyyx,    -Sxx+Syy-Szz,    Syzzy; ...
          Sxy_yx,       Szxxz,        Syzzy,   -Sxx-Syy+Szz ];

  % Eigen Analisys
  [V, D] = eig ( N );
  % Look for the biggest 
  [MaxEigV, MaxEigI] = max ( diag ( D ) );

  % Eigen values are sorted
  if MaxEigV > 0 && isreal ( MaxEigV ),
    % Vector associated to the biggest eigen value => unitary quaternion
    q = V(:,MaxEigI);

    % Translation = difference between centroid of right measurement and the scaled and
    %               rotated centroid of the left measurement
    R = quat2rotmatMx ( q );

    if ScaleType == 1,
      % Compute Scale (Symmetric Scale):
      Sl = sum ( sum ( l .* l ) );
      Sr = sum ( sum ( r .* r ) );
      s = sqrt ( Sr / Sl );
    elseif ScaleType == 2,
      % Compute Scale (Non-Symmetric Scale):
      s = sum ( sum ( r .* ( R * r ), 1 ) ) / sum ( sum ( r .* r, 1 ) );
    elseif ScaleType == 0
      % No Scaling factor
      s = 1;
    else
      error ( 'MATLAB:quatAbsOrientation:Input', ...
              'ScaleType must be either 0 (No Scale), 1 (Symmetric Scale) or 2 (Non-Symmetric Scale)!' );
    end

    % Translation = difference between centroid of right measurement and the scaled and
    %               rotated centroid of the left measurement
    t = rr - s * R * rl;
  else
    % Error => q corresponds to the vector associated to the biggest positive eigen value
    warning ( 'MATLAB:quatAbsOrientation:Error', ...
              'M is not rank 3, points are collinear!' );
    R = [];
    t = [];
    s = [];
  end
end
