%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Test Conversions from Unit Quaternion to 3-Vectors
%                  and vice versa.
%
%  File          : TestQ2V.m
%  Date          : 05/03/2009 - 05/03/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Number of Tests
n = 100000;
% Epsilon to check errors
Epsilon = 1e-12;
% Flags
DoTestQuat2Vec = true;
DoTestVec2Quat = true;


if DoTestQuat2Vec;
  fprintf ( 'Allocating storage for testing from random Normalized Quaternion -> 3-Vector...\n' );

  % Generate a random Quaternion
  qs = rand ( 4, n );
  % Allocate space for results
  qd = zeros ( 4, n );

  fprintf ( 'Executing %d tests...\n', n );
  for i = 1 : n;
    % Normalize current quaternion
    q0 = qs(:,i) / norm ( qs(:,i) );
    qs(:,i) = q0;

    % Convert to 3-Vector
    v = quat2vecMx ( q0 );

    % Convert back to Quaternion
    q = vec2quatMx ( v );

    % Accumulate in a Vector
    qd(:,i) = q;
  end


  fprintf ( 'Computing errors...\n' );
  Ns = zeros ( 1, n );
  for i = 1:n;
    Ns(i) = norm ( qd(:,i) - qs(:,i), 'fro' );
  end


  I = find ( Ns > Epsilon );
  if size ( I, 2 );
    fprintf ( '  Printing Index of (%d) conversions out of error bounds (eps = %e)...\n', size ( I, 2 ), Epsilon );
    disp ( I );
  else
    disp ( '  The error of ALL convertions are in bounds!' );
  end
end


if DoTestVec2Quat;
  fprintf ( '\nAllocating storage for testing from random 3-Vector -> Normalized Quaternion...\n' );

  % Generate a random 3-Vectors
  vs = rand ( 3, n );
  % Allocate space for results
  vd = zeros ( 3, n );


  GoodCnvs = 0;
  fprintf ( 'Executing %d tests...\n', n );
  for i = 1 : n;
    % Normalize current quaternion
    v0 = vs(:,i);

    % Convert back to Quaternion
    q = vec2quatMx ( v0 );
    if isfinite ( q(1) );
      GoodCnvs = GoodCnvs + 1;
    end
    
    % Convert to 3-Vector
    v = quat2vecMx ( q );

    % Accumulate in a Vector
    vd(:,i) = v;
  end
  
  fprintf ( '  Executed %d right convertions to unit quaternion out of %d input random 3-vector...\n', GoodCnvs, n );


  fprintf ( 'Computing errors...\n' );
  Ns = zeros ( 1, n );
  for i = 1:n;
    Ns(i) = norm ( vs(:,i) - vd(:,i), 'fro' );
  end
  
  
  I = find ( Ns > Epsilon );
  r = numel ( I );
  if r > 0;
    fprintf ( '  Printing Index of (%d) conversions out of error bounds (eps = %e)!\n', r, Epsilon );
    disp ( I );
  else
    disp ( '  The error of ALL convertions is in bounds!' );
  end
end
