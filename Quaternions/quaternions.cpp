/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Quaternion / Rotation Matrix generic routines.

  File          : quaternions.cpp
  Date          : 23/02/2007 - 05/03/2009

  Compiler      : g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2009 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <cmath>

#include "quaternions.h"

// Define the macro to use (normally, more accurate):
//   Eugene Salamin
//   Application of quaternions to computation with rotations
//   Stanford Internal Technical Report, 1979
//
#define __USE_SALAMIN__

// Epsilon to considere 0
#define __QEPSILON__  1e-8

// Cross Product between vectors
#define CROSSi(a,b) ( a[2] * b[3] - a[3] * b[2] )
#define CROSSj(a,b) ( a[3] * b[1] - a[1] * b[3] )
#define CROSSk(a,b) ( a[1] * b[2] - a[2] * b[1] )
        
// Copy quaternion
//
void quatcopy ( const double qs[4], double qd[4] )
{
  qd[0] = qs[0];
  qd[1] = qs[1];
  qd[2] = qs[2];
  qd[3] = qs[3];
}

// Quaternion Normalization (input variable)
//
void quatnorm ( double q[4] )
{
  double N = sqrt ( q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3] );

  q[0] /= N;
  q[1] /= N;
  q[2] /= N;
  q[3] /= N;
}

// Quaternion Normalization (with copy)
//
void quatnorm ( const double qs[4], double qd[4] )
{
  quatcopy ( qs, qd );
  quatnorm ( qd );
}

// Quaternion Conjugate
//
void quatconj ( double q[4] )
{
    q[1] = -q[1];
    q[2] = -q[2];
    q[3] = -q[3];
}
        
// Quaternion Conjugate (with copy)
//
void quatconj ( const double qs[4], double qd[4] )
{
  quatcopy ( qs, qd );
  quatconj ( qd );
}

// Quaternion to 3-Vector
//
void quat2vec ( const double q[4], double v[3] )
{
  // Normalize quaternion ensuring positive scalar component, if not,
  // change sign since q and -q is represent the same rotation
//  double Magn = sqrt ( q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3] );
//  double Sign = q[0] >= 0.0 ? 1.0 : -1.0;
//  Magn = Sign / Magn;
//  v[0] = q[1] * Magn;
//  v[1] = q[2] * Magn;
//  v[2] = q[3] * Magn;
  
  //  Assume normalized quaternion
  double Sign = q[0] >= 0.0 ? 1.0 : -1.0;

  v[0] = q[1] * Sign;
  v[1] = q[2] * Sign;
  v[2] = q[3] * Sign;
}

// 3-Vector to Quaternion
//
void vec2quat ( const double v[3], double q[4] )
{
  // Recover the scalar part of a quaternion encoded in the vector (length)
  q[0] = sqrt ( 1.0 - v[0] * v[0] - v[1] * v[1] - v[2] * v[2] );
  q[1] = v[0];
  q[2] = v[1];
  q[3] = v[2];
}

// Convert a 3D point into a quaternion
//
void p3D2quat ( const double p[3], double q[4] )
{
  q[0] = 0.0;
  q[1] = p[0];
  q[2] = p[1];
  q[3] = p[2];
}

// Invert a Quaternion
//
void quatinv ( double q[4] )
{
  // q^-1 = q' / ( q * q' )
  // q^-1 = q' (if q is normalized)
  quatnorm ( q );
  quatconj ( q );
}

// Invert a Quaternion (whith copy)
//
void quatinv ( const double qs[4], double qd[4] )
{
  quatcopy ( qs, qd );
  quatinv ( qd );
}

// Rotate a point p = (x,y,z) by a quaternion specified in a 3-Vector v
//   t = ( 0, p )
//   q = ( sqrt ( 1 - v0^2 - v1^2 - v2^2 ), v0, v1, v2 )
//   r = q * t * q^-1 = q * t * qc
//
// Refs: http://www.genesis3d.com/~kdtop/Quaternions-UsingToRepresentRotation.htm
//       http://en.wikipedia.org/wiki/Cross_product
//
void quatrot ( const double v[3], const double p[3], double r[4] )
{
  double q[4];
  double qc[4];
  double t[4];
  double qt[4];

  // From 3D point to quaternion
  p3D2quat ( p, t );
  // From 3-Vector to quaternion
  vec2quat ( v, q );
  // Invert by using the conjugate since q is normalized
  quatconj ( q, qc );

  // Quaternion Product
  //   q1 = (s1, v1)
  //   q2 = (s2, v2)
  //   q1 * q2 = ( s1*s2 - v1 * v2, s1*v2 + s2*v1 + v1 x v2 )

  // Execute product using the following Knowledge
  //   q  = ( s1,  v1 )
  //   t  = (  0,  v2 )
  //   qc = ( s1, -v1)

  //             |  i  j  k |
  // a x b = det | a1 a2 a3 | = (a2 * b3 - a3 * b2 )i + (a3 * b1 - a1 * b3)j + (a1 * b2 - a2 * b1)k
  //             | b1 b2 b3 |
        
  // (q*t)*qc
  //        q * t = ( 0 - v1 * v2, s1 * v2 + v1 x v2 )
  //   q * t * qc = ( -v1 * v2 * s1 + ( s1 * v2 + v1 x v2 ) * v1,  -v1 * v2 * v2 + s1 * ( s1 * v2 + v1 x v2 ) + v1 x (s1 * v2 + v1 x v2) )
  qt[0] = -q[1] * t[1] - q[2] * t[2] - q[3] * t[3];
  qt[1] = q[0] * t[1] + CROSSi(q,t);
  qt[2] = q[0] * t[2] + CROSSj(q,t);
  qt[3] = q[0] * t[3] + CROSSk(q,t);

  // Normal Product
//  quatprod ( q, t, qt );

  quatprod ( qt, qc, r );
}

// Quaternion Product
//
void quatprod ( const double q1[4], const double q2[4], double qd[4] )
{
  double q01, qx1, qy1, qz1, q02, qx2, qy2, qz2;

  q01 = q1[0]; qx1 = q1[1]; qy1 = q1[2]; qz1 = q1[3];
  q02 = q2[0]; qx2 = q2[1]; qy2 = q2[2]; qz2 = q2[3];

  qd[0] = q01 * q02 - qx1 * qx2 - qy1 * qy2 - qz1 * qz2;
  qd[1] = q01 * qx2 + qx1 * q02 + qy1 * qz2 - qz1 * qy2;
  qd[2] = q01 * qy2 - qx1 * qz2 + qy1 * q02 + qz1 * qx2;
  qd[3] = q01 * qz2 + qx1 * qy2 - qy1 * qx2 + qz1 * q02;
}


#ifdef __USE_SALAMIN__

  // Convertion From Quaternion to Rotation Matrix
  //
  // Salamin '79
  //
  void quat2rotmat ( const double q[4], double R[9] )
  {
    //     |  q02+qx2-qy2-qz2         -q0z+qxy          q0y+qxz |   | r11 r12 r13 |
    // R = |          q0z+qxy  q02-qx2+qy2-qz2         -q0x+qyz | = | r21 r22 r23 |
    //     |         -q0y+qxz          q0x+qyz  q02-qx2-qy2+qz2 |   | r31 r32 r33 |
    //
    double q0, qx, qy, qz;
    double q02, qx2, qy2, qz2;
    double q0x, q0y, q0z, qxy, qxz, qyz;

    // Get the quaternion
    q0 = q02 = q[0];
    qx = qx2 = q[1];
    qy = qy2 = q[2];
    qz = qz2 = q[3];

    // Compute squares
    q02 *= q02;
    qx2 *= qx2;
    qy2 *= qy2;
    qz2 *= qz2;

  /*
    // Normalize                            
    double N, N2;
    N = N2 = sqrt ( q02 + qx2 + qy2 + qz2 );
    N2 *= N2;

    q0 /= N;
    qx /= N;
    qy /= N;
    qz /= N;
    q02 /= N2;
    qx2 /= N2;
    qy2 /= N2;
    qz2 /= N2;
  */

    // Terms for non diagonals
    q0x = 2.0 * q0 * qx;
    q0y = 2.0 * q0 * qy;
    q0z = 2.0 * q0 * qz;
    qxy = 2.0 * qx * qy;
    qxz = 2.0 * qx * qz;
    qyz = 2.0 * qy * qz;

    // Compute the rotation matrix
    R[_R11_] =  q02+qx2-qy2-qz2;
    R[_R12_] = -q0z+qxy;
    R[_R13_] =  q0y+qxz;
    R[_R21_] =  q0z+qxy;
    R[_R22_] =  q02-qx2+qy2-qz2;
    R[_R23_] = -q0x+qyz;
    R[_R31_] = -q0y+qxz;
    R[_R32_] =  q0x+qyz;
    R[_R33_] =  q02-qx2-qy2+qz2;
  }

  // Convertion from Rotation Matrix to Quaternion
  //
  // Salamin '79
  //
  void rotmat2quat ( const double R[9], double q[4] )
  {
    double R11, R12, R13, R21, R22, R23, R31, R32, R33;

    // Rotation Matrix
    R11 = R[_R11_]; R12 = R[_R12_]; R13 = R[_R13_];
    R21 = R[_R21_]; R22 = R[_R22_]; R23 = R[_R23_];
    R31 = R[_R31_]; R32 = R[_R32_]; R33 = R[_R33_];

    q[0] = ( 1.0 + R11 + R22 + R33 ) / 4.0;
    q[1] = ( 1.0 + R11 - R22 - R33 ) / 4.0;
    q[2] = ( 1.0 - R11 + R22 - R33 ) / 4.0;
    q[3] = ( 1.0 - R11 - R22 + R33 ) / 4.0;

    // Compute the maximum qi
    double MaxVal = q[0];
    int Index = 0;
    int i = 1;
    while ( MaxVal < ( 1.0 / 4.0 ) && i < 4 )
    {
      if ( q[i] > MaxVal )
      {
        MaxVal = q[i];
        Index = i;
      }

      i++;
    }

    double qi = sqrt ( MaxVal );

    switch ( Index )
    {
      case 0:
        q[0] = qi;
        q[1] = ( ( R32 - R23 ) / 4.0 ) / qi;
        q[2] = ( ( R13 - R31 ) / 4.0 ) / qi;
        q[3] = ( ( R21 - R12 ) / 4.0 ) / qi;
        break;

      case 1:
        q[0] = ( ( R32 - R23 ) / 4.0 ) / qi;
        q[1] = qi;
        q[2] = ( ( R12 + R21 ) / 4.0 ) / qi;
        q[3] = ( ( R13 + R31 ) / 4.0 ) / qi;
        break;

      case 2:
        q[0] = ( ( R13 - R31 ) / 4.0 ) / qi;
        q[1] = ( ( R12 + R21 ) / 4.0 ) / qi;
        q[2] = qi;
        q[3] = ( ( R23 + R32 ) / 4.0 ) / qi;
        break;

      case 3:
        q[0] = ( ( R21 - R12 ) / 4.0 ) / qi;
        q[1] = ( ( R13 + R31 ) / 4.0 ) / qi;
        q[2] = ( ( R23 + R32 ) / 4.0 ) / qi;
        q[3] = qi;
        break;

      default:
        break;
    }
  }

#else

  // Convertion From Quatertion to Rotation Matrix
  //
  // Id Software, http://cache-www.intel.com/cd/00/00/29/37/293748_293748.pdf
  //
  void quat2rotmat ( const double q[4], double R[9] )
  {
    //      | 1 - 2qy^2 - 2qz^2     2qxy + 2q0z        2qxz - 2q0y    |   | r11 r12 r13 |
    // R' = |    2qxy - 2q0z     1 - 2qx^2 - 2qz^2     2qyz + 2q0x    | = | r21 r22 r23 |
    //      |    2qxz + 2q0y        2qyz - 2q0x     1 - 2qx^2 - 2qy^2 |   | r31 r32 r33 |

    double q0, qx, qy, qz;
    double qx2, qy2, qz2;
    double q0x, q0y, q0z, qxy, qxz, qyz;

    // Get the quaternion
    q0 =       q[0];
    qx = qx2 = q[1];
    qy = qy2 = q[2];
    qz = qz2 = q[3];

    // Compute 2.0 * squares
    qx2 *= 2.0 * qx2;
    qy2 *= 2.0 * qy2;
    qz2 *= 2.0 * qz2;

    // Terms for non diagonals
    q0x = 2.0 * q0 * qx;
    q0y = 2.0 * q0 * qy;
    q0z = 2.0 * q0 * qz;
    qxy = 2.0 * qx * qy;
    qxz = 2.0 * qx * qz;
    qyz = 2.0 * qy * qz;

    // Compute the rotation matrix (Notice that the rotation is transposed!)
    R[_R11_] = 1.0 - qy2 - qz2;
    R[_R21_] = qxy + q0z;
    R[_R31_] = qxz - q0y;
    R[_R12_] = qxy - q0z;
    R[_R22_] = 1.0 - qx2 - qz2;
    R[_R32_] = qyz + q0x;
    R[_R13_] = qxz + q0y;
    R[_R23_] = qyz - q0x;
    R[_R33_] = 1.0 - qx2 - qy2;
  }

  // Convertion from Rotation Matrix to Quaternion
  //
  //  http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
  //
  void rotmat2quat ( const double R[9], double q[4] )
  {
/*
   double w = sqrt ( ( R[_R11_] + R[_R22_] + R[_R33_] + 1.0 ) ) / 2.0;

    q[0] = w;
    q[1] = ( R[_R32_] - R[_R23_] ) / ( w * 4.0 );
    q[2] = ( R[_R13_] - R[_R31_] ) / ( w * 4.0 );
    q[3] = ( R[_R21_] - R[_R12_] ) / ( w * 4.0 );
*/

    // More Accurate Implementation
    
    double Trace = 1.0 + R[_R11_] + R[_R22_] + R[_R33_];
    double s;

    if ( Trace > __QEPSILON__ )
    {
      s = 0.5 / sqrt ( Trace );
      q[0] = 0.25 / s;
      q[1] = ( R[_R32_] - R[_R23_] ) * s;
      q[2] = ( R[_R13_] - R[_R31_] ) * s;
      q[3] = ( R[_R21_] - R[_R12_] ) * s;
    }
    else
    {
      if ( R[_R11_] > R[_R22_] && R[_R11_] > R[_R33_] )
      {
        s = 2.0 * sqrt ( 1.0 + R[_R11_] - R[_R22_] - R[_R33_] );
        q[0] = ( R[_R23_] - R[_R32_] ) / s;
        q[1] = 0.25 * s;
        q[2] = ( R[_R12_] + R[_R21_] ) / s;
        q[3] = ( R[_R13_] + R[_R31_] ) / s;
      }
      else
        if ( R[_R22_] > R[_R33_] )
        {
          s = 2.0 * sqrt ( 1.0 + R[_R22_] - R[_R11_] - R[_R33_] );
          q[0] = ( R[_R13_] - R[_R31_] ) / s;
          q[1] = ( R[_R12_] + R[_R21_] ) / s;
          q[2] = 0.25 * s;
          q[3] = ( R[_R23_] + R[_R32_] ) / s;
        }
        else
        {
          s = 2.0 * sqrt( 1.0 + R[_R33_] - R[_R11_] - R[_R22_] );
          q[0] = ( R[_R12_] - R[_R21_] ) / s;
          q[1] = ( R[_R13_] + R[_R31_] ) / s;
          q[2] = ( R[_R23_] + R[_R32_] ) / s;
          q[3] = 0.25 * s;
        }
    }
  }

#endif
