%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Test Conversions from Rotation Matrix to Unit Quaternion
%                  and vice versa.
%
%  File          : TestQ2RM.m
%  Date          : 04/06/2008 - 05/03/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Number of Tests
n = 10000;
% Epsilon to check errors
Epsilon = 1e-8;


fprintf ( 'Allocating storage for tests...\n' );

A  = zeros ( 3, 3, n );
AA = zeros ( 3, 3, n );
qa = zeros ( 4, n );

I = eye ( 3 );

fprintf ( 'Executing %d tests...\n', n );
for i = 1 : n;
  % Generate a random Rotation Matrix which not mirrors (det > 0)
  NotOk = true;
  while NotOk;
    % Random
    R = rand ( 3, 3 );
    % Normalize
    An = R*(R'*R)^-0.5;

    % Check Det => +1
    NotOk = det ( An ) < 0;
  end

  % Norm of all vectors
  c(1) = norm ( An(:,1) );
  c(2) = norm ( An(:,2) );
  c(3) = norm ( An(:,3) );
  c(4) = norm ( An(1,:) );
  c(5) = norm ( An(2,:) );
  c(6) = norm ( An(3,:) );

  % Check Norm to be 1 +/- Epsilon
  cc = abs(c) - 1;
  cceps = cc > Epsilon;

  % R' * R = 1
  I1 = An * An';
  I2 = An' * An;

  % Frobbenius norm
  i1 = norm ( I - I1, 'fro' );
  i2 = norm ( I - I2, 'fro' );

  % Check that is a right rotation matrix
  if rank ( An ) ~= 3 || any ( cceps ) || i1 > Epsilon || i2 > Epsilon 
    fprintf ( '  Matrix %d not belonging to SO(3)!\n', i );
  end

  % Convert to Quaternion
  qan = rotmat2quatMx ( An );
  % Convert back to Rotation Matrix
  AAn = quat2rotmatMx ( qan );

  % Accumulate in a Vector
  qa(:,i) = qan;
  A(:,:,i) = An;
  AA(:,:,i) = AAn;
end


fprintf ( 'Computing errors...\n' );
Ns = zeros ( 1, n );
for i = 1:n;
  Ns(i) = norm ( A(:,:,i) - AA(:,:,i), 'fro' );
end


I = find ( Ns > Epsilon );
r = numel ( I );
if r > 0;
  fprintf ( '  Printing Index of (%d) conversions out of error bounds (eps = %e)!\n', r, Epsilon );
  disp ( I );
else
  disp ( '  The error of ALL convertions is in bounds!' );
end
