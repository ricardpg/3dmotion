/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Rotation Matrix to Quaterniont convertion.

  File          : rotmat2quatMx.cpp
  Date          : 23/02/2007 - 05/03/2009

  Compiler      : g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2005-2009 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <mex.h>            // Matlab Mex Functions

#include <quaternions.h>

// Define for strict checking of the input/output parameters (slower)
// #define DO_STRICT_CHECK

#define ERROR_HEADER  "MATLAB:rotmat2quatMx:Input\n"

// Identify the input parameters by it's index
enum { R = 0 };
// Identify the output parameters by it's index
enum { Q = 0 };

// Mex function
//
void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter count checks
#ifdef DO_STRICT_CHECK
  if ( nlhs != 1 || nrhs != 1 )
#else
  if ( nrhs != 1 )
#endif
    mexErrMsgTxt ( ERROR_HEADER "Usage: q = rotmat2quatMx ( R )" );

#ifdef DO_STRICT_CHECK
  // Get the File Name lenghts
  int r = mxGetM ( prhs[R] );
  int c = mxGetN ( prhs[R] );  

  if ( r != 3 || c != 3 )
    mexErrMsgTxt ( ERROR_HEADER "Input matrix must be a 3x3 Rotation Matrix!" );
#endif

  // Create Rotation Matrix Space
  plhs[Q] = mxCreateDoubleMatrix ( 4, 1, mxREAL );

  // Get the pointers
  double *Rm = (double *)mxGetPr ( prhs[R] );
  double *q = (double *)mxGetPr ( plhs[Q] );

  // Compute
  rotmat2quat ( Rm, q );
}
