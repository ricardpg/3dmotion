%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Generate Matlab Mex Files.
%
%  File          : Compile.m
%  Date          : 23/02/2007 - 27/10/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%


% Check for GSL library usage
UserGsl = '';
while ~(strcmpi ( UserGsl, 'y' ) || strcmpi ( UserGsl, 'n' ));
  fprintf ( '\n' );
  UserGsl = input ( [ 'Do you have GNU gsl (http://www.gnu.org/software/gsl/) installed?\n' ...
                      'If you answer ''N'' a low performance Matlab implementation (quatAbsOrientation) will be\n' ...
                      'used instead of the Mexified version of Absolute Orientation Algorithm (quatAbsOrientationMx) [Y/N]: '], 's' );
end

if strcmpi ( UserGsl, 'y' );
  DoUseGSL = true;
else
  DoUseGSL = false;
end

% Path
QuaternionsFolder = '.';

% Flags
Flags{1} = 'DO_STRICT_CHECK';   % Force strict checks in passed parameters to the mex files
% Flags{2} = 'DO_DEBUG';        % Show Debug Information

% Files to Compile
clear ( 'FileNames' );
FileNames{1} = 'quat2rotmatMx.cpp';
FileNames{2} = 'rotmat2quatMx.cpp';
FileNames{3} = 'quat2vecMx.cpp';
FileNames{4} = 'vec2quatMx.cpp';
FileNames{5} = 'quatprodMx.cpp';
FileNames{6} = 'quatrotMx.cpp';
if DoUseGSL;
  FileNames{7} = 'quatAbsOrientationMx.cpp -lgsl';

  fprintf ( 2, 'DoUseGSL is enabled. If compilation fails (normally for Windows users) re-run this script and answer N (low performance) to the previous question!\n' );
else
  fprintf ( 2, [ 'DoUseGSL is NOT enabled. If you have GSL installed in your system and you want to use a Mexified version of\n' ...
                 'Absolute Orientation Algorithm (high performance), re-run this script and answer Y to the previous question!\n' ] );
end
fprintf ( '\n' );

% Compatibility list (to use without GSL)
CmpFileNames{1,1} = [ QuaternionsFolder '/' 'quatAbsOrientationMx.m' ];  CmpFileNames{1,2} = [ QuaternionsFolder '/' 'quatAbsOrientation.m' ];

% Shared Files
SharedFile = 'quaternions.cpp';

% Compose Flags
DFlags = '';
if exist ( 'Flags', 'var' ) && iscell ( Flags );
  fprintf ( '  Using preprocessing flags:' );
  for i = 1 : size ( Flags, 2 );
    if ~isempty ( Flags{i} ); 
      DFlags = [ DFlags ' -D' Flags{i} ];
      fprintf ( ' ''%s''', Flags{i} );
    end
  end
  fprintf ( '\n' );
else
  disp ( '  Not using any preprocessor flag...' );
end

% Compile
for i = 1 : size ( FileNames, 2 );
  fprintf ( '  Compiling "%s"\n', FileNames{i} );
  Cmd = [ 'mex ' DFlags ' -I' QuaternionsFolder ' ' FileNames{i} ' ' QuaternionsFolder '/' SharedFile ];
  fprintf ( '    %s\n', Cmd );
  eval ( Cmd );
end

if ~DoUseGSL;
  fprintf ( '\n' );
  fprintf ( '  Replacing mex files (DoUSEGSL=false) with compatible ones...\n' );
  for i = 1 : size ( CmpFileNames, 1 );
    fprintf ( '    Copying ''%s'' -> ''%s'':\n', CmpFileNames{i,2}, CmpFileNames{i,1} );
    [Success Message MessageId] = copyfile ( CmpFileNames{i,2}, CmpFileNames{i,1} );
    if Success;
      fprintf ( '      OK!' );
    else
      fprintf ( 2, '\n     %s: ''%s''', MessageId, Message );
    end
    fprintf ( '\n' );
  end
end
