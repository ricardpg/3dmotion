/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Convert a scaled 3-vector to a quaternion.

  File          : vec2quatMx.cpp
  Date          : 05/03/2009 - 05/03/2009

  Compiler      : g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2009 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <mex.h>            // Matlab Mex Functions

#include <quaternions.h>

// Define for strict checking of the input/output parameters (slower)
// #define DO_STRICT_CHECK

#define ERROR_HEADER  "MATLAB:vec2quatMx:Input\n"
        
// Identify the input parameters by it's index
enum { V = 0 };
// Identify the output parameters by it's index
enum { Q = 0 };

// Mex function
//
void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter count checks
#ifdef DO_STRICT_CHECK
  if ( nlhs != 1 || nrhs != 1 )
#else
  if ( nrhs != 1 )
#endif
    mexErrMsgTxt ( ERROR_HEADER "Usage: q = quat2vecMx ( v )" );

#ifdef DO_STRICT_CHECK
  // Get input lenghts
  int r = mxGetM ( prhs[V] );
  int c = mxGetN ( prhs[V] );  

  if ( ( r != 1 || c != 3 ) && ( r != 3 || c != 1 ) )
    mexErrMsgTxt ( ERROR_HEADER "Input vector must be 1x4 or 4x1 quaternion!" );
#endif

  // Create Output Quaternion
  plhs[Q] = mxCreateDoubleMatrix ( 4, 1, mxREAL );

  // Get the pointers
  double *v = (double *)mxGetPr ( prhs[V] );
  double *q = (double *)mxGetPr ( plhs[Q] );

  // Compute
  vec2quat ( v, q );
}
