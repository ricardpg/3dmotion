%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compile all needed in the project.
%
%  File          : Compile.m
%  Date          : 04/06/2008 - 27/10/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

FolderNames{1} = 'HomoTrans';
FolderNames{2} = 'Quaternions';
FolderNames{3} = 'Rotations';

CompilationFileName = 'Compile';

% Compile everything
%
for i = 1 : size ( FolderNames, 2 );
  CmpFilePath = [ FolderNames{i} '/' CompilationFileName ];
  fprintf ( 'Processing folder "%s"...\n', FolderNames{i} );
  if exist ( [ CmpFilePath '.m' ], 'file' )
    fprintf ( ' Executing "%s"...\n', CmpFilePath );
    eval ( [ 'cd ' FolderNames{i} ] );
    Compile
    eval ( 'cd ..' );
  else
    disp ( '  Nothing to do...' );
  end
end

% Add Paths Automatically
UserPath = '';
while ~(strcmpi ( UserPath, 'y' ) || strcmpi ( UserPath, 'n' ));
  fprintf ( '\n' );
  UserPath = input ( 'Do you want to set and save the path to 3DMotion subfolders [Y/N]? ', 's' );
end

if strcmpi ( UserPath, 'y' );
  % Set the path
  path ( [cd '/HomoTrans'], path );
  path ( [cd '/Quaternions'], path );
  path ( [cd '/Rotations'], path );
  % Save the path
  savepath
else
  fprintf ( 2, 'Remeber to set the path to 3DMotion project in order to use its functions!\n' );
end
