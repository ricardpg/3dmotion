%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : Delaunay Triangulation Functions
%  Homepage      : http://uvl.udg.edu
%
%  Module        : Test Function for Absolute Orientation Algorithm.
%
%  File          : TestAbsOr.m
%  Date          : 08/05/2009 - 08/05/2009
%  Encoding      : ISO-8859-1 (Latin-1)
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         :
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

DoTestInvHomoPose = true;              % Test the mex implementation of Homo Transformations
DoTestApplyHomoToPosition = true;
  DoGenerateData = true;
  DoTestMatlab = true;
  DoTestMx = true;
  DoShowResults = true;
  NumMatrices = 30000;
  NumPoints = 6000000;
  ErrEps = 10^-10;
  TRange = [100; 20; 50];              % Translation Range
  TOffs = [-1000; 200; 100];           % Translation Offset


if DoTestInvHomoPose;
  fprintf ( 'Testing InvHomoPose/InvHomoPoseMx...\n' );
  if DoGenerateData;
    fprintf ( '  Generating %d Homogenous Matrices...\n', NumMatrices );

    t = rand ( 3, NumMatrices );
    t = t .* repmat ( TRange, 1, size ( t, 2 ) ) + repmat ( TOffs, 1, size ( t, 2 ) );
    R = rand ( 3, 3, NumMatrices );

    SrcHomo = zeros ( 4, 4, NumMatrices );
    for i = 1 : NumMatrices;
      R(:,:,i) = R(:,:,i)*(R(:,:,i)'*R(:,:,i))^-0.5;
      if det ( R(:,:,i) ) < 0;
        R(:,:,i) = -R(:,:,i);
      end
      SrcHomo(:,:,i) = [R(:,:,i) t(:,i); [ 0 0 0 1]];
    end

  end

  if DoTestMatlab;
    fprintf ( '  Testing (%d elements) Matlab''s Implementation...\n', NumMatrices );
    InitTime = cputime;
    DstHomo = InvHomoPose ( SrcHomo );
    EndTime = cputime;
    Time = EndTime - InitTime;
    fprintf ( '    Time %fs (%fh)\n', Time, Time / 3600 );
  end

  if DoTestMx;
    fprintf ( '  Testing (%d elements) C++ Mex Implementation...\n', NumMatrices );
    InitTime = cputime;
    DstHomoMx = InvHomoPoseMx ( SrcHomo );
    EndTime = cputime;
    Time = EndTime - InitTime;
    fprintf ( '    Time %fs (%fh)\n', Time, Time / 3600 );
  end

  if DoShowResults;
    fprintf ( '  Comparing results (Max allowed Error %e)...\n', ErrEps );
    ErrR = zeros ( 1, NumMatrices );
    for i = 1 : NumMatrices;
      ErrR(i) = norm ( DstHomo(:,:,i) - DstHomoMx(:,:,i), 'fro' );
    end

    % Compute Number of Errors
    Err = ErrR > ErrEps;
    NumErrors = sum ( Err ~= 0 );

    % Compute Error Range
    MaxErr = max ( ErrR );
    MinErr = min ( ErrR );

    % Plot Results
    if NumErrors > 0;
      fprintf ( '    %d Errors found in %d elements (Eps = %e)!\n', NumErrors, NumMatrices, ErrEps );
    else
      fprintf ( '    No Errors found in %d elements!\n', NumMatrices );
    end

    fprintf ( '    Error range (%e, %e)!\n', MinErr, MaxErr );
  end
else
  warning ( 'MATLAB:TestHomo', 'Skiping test of InvHomoPose/InvHomoPoseMx!' );
end


if DoTestApplyHomoToPosition;
  fprintf ( '\n' );
  fprintf ( 'Testing ApplyHomoToPosition/ApplyHomoToPositionMx...\n' );
  if DoGenerateData;
    fprintf ( '  Generating %d 3D Random Points...\n', NumPoints );
    P = rand ( 3, NumPoints );
    P = P .* repmat ( TRange, 1, size ( P, 2 ) ) + repmat ( TOffs, 1, size ( P, 2 ) );

    fprintf ( '  Generating a 4x4 Homogeneous Transformation ...\n' );
    R = rand ( 3, 3 );
    R = R*(R'*R)^-0.5;      
    if det ( R ) < 0;
      R = -R;
    end
    t = rand ( 3, 1 );
    t = t .* repmat ( TRange, 1, size ( t, 2 ) ) + repmat ( TOffs, 1, size ( t, 2 ) );
    M = [R t; [ 0 0 0 1]];
  end
  
  if DoTestMatlab;
    fprintf ( '  Testing (%d elements) Matlab''s Implementation...\n', NumPoints );
    InitTime = cputime;
    Pr = ApplyHomoToPosition ( Homo, P );
    EndTime = cputime;
    Time = EndTime - InitTime;
    fprintf ( '    Time %fs (%fh)\n', Time, Time / 3600 );
  end

  if DoTestMx;
    fprintf ( '  Testing (%d elements) Matlab''s Implementation...\n', NumPoints );
    InitTime = cputime;
    PrMx = ApplyHomoToPositionMx ( Homo, P );
    EndTime = cputime;
    Time = EndTime - InitTime;
    fprintf ( '    Time %fs (%fh)\n', Time, Time / 3600 );
  end

  if DoShowResults;
    fprintf ( '  Comparing results (Max allowed Error %e)...\n', ErrEps );
    ErrR = zeros ( 1, NumMatrices );
    for i = 1 : NumMatrices;
      ErrR(i) = norm ( Pr(:,i) - PrMx(:,i) );
    end

    % Compute Number of Errors
    Err = ErrR > ErrEps;
    NumErrors = sum ( Err ~= 0 );

    % Compute Error Range
    MaxErr = max ( ErrR );
    MinErr = min ( ErrR );

    % Plot Results
    if NumErrors > 0;
      fprintf ( '    %d Errors found in %d elements (Eps = %e)!\n', NumErrors, NumPoints, ErrEps );
    else
      fprintf ( '    No Errors found in %d elements!\n', NumPoints );
    end

    fprintf ( '    Error range (%e, %e)!\n', MinErr, MaxErr );
  end
else
  warning ( 'MATLAB:TestHomo', 'Skiping test of ApplyHomoToPose/ApplyHomoToPoseMx!' );
end