%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Generate Matlab Mex Files.
%
%  File          : Compile.m
%  Date          : 08/05/2007 - 08/05/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Path
HomoTransFolder = '.';

% Flags
% Flags{1} = 'DO_STRICT_CHECK';   % Force strict checks in passed parameters to the mex files
% Flags{2} = 'DO_DEBUG';          % Show Debug Information

% Files to Compile
clear ( 'FileNames' );
FileNames{1} = 'InvHomoPoseMx.cpp';
FileNames{2} = 'ApplyHomoToPositionMx.cpp';

% Compose Flags
DFlags = '';
if exist ( 'Flags', 'var' ) && iscell ( Flags );
  fprintf ( '  Using preprocessing flags:' );
  for i = 1 : size ( Flags, 2 );
    if ~isempty ( Flags{i} ); 
      DFlags = [ DFlags ' -D' Flags{i} ];
      fprintf ( ' ''%s''', Flags{i} );
    end
  end
  fprintf ( '\n' );
else
  disp ( '  Not using any preprocessor flag...' );
end

% Compile
for i = 1 : size ( FileNames, 2 );
  fprintf ( '  Compiling "%s"\n', FileNames{i} );
  Cmd = [ 'mex ' DFlags ' -I' HomoTransFolder ' ' FileNames{i} ];
  fprintf ( '    %s\n', Cmd );
  eval ( Cmd );
end
