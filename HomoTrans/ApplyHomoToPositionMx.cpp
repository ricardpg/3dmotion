/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Absolute Orientation problem using quaternions.

  File          : ApplyHomoToPositionMx.cpp
  Date          : 08/05/2009 - 08/05/2009

  Compiler      : g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2009 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <cmath>            // STL
#include <iostream>
#include <mex.h>            // Matlab Mex Functions

// Define for strict checking of the input/output parameters (slower)
// #define DO_STRICT_CHECK
// To ouptut Debug Information
// #define DO_DEBUG

#define ERROR_HEADER  "MATLAB:ApplyHomoToPositionMx:Input\n"

// Identify the input parameters by it's index
enum { M = 0, P };
// Identify the output parameters by it's index
enum { R = 0 };

// Mex function
//
void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter count checks
#ifdef DO_STRICT_CHECK
  if ( ( nrhs != 2 ) || ( nlhs != 1 ) )
#else
  if ( nrhs != 2 )
#endif
    mexErrMsgTxt ( ERROR_HEADER "Usage:  R = ApplyHomoToPositionMx ( M, P )" );

  if ( mxGetNumberOfDimensions ( prhs[M] ) != 2 || !mxIsDouble ( prhs[M] ) )
    mexErrMsgTxt ( ERROR_HEADER "M must be a 4x4 Homogeneous matrix!" );

  if ( mxGetNumberOfDimensions ( prhs[P] ) != 2 || !mxIsDouble ( prhs[P] ) )
    mexErrMsgTxt ( ERROR_HEADER "P must be a double matrix containing 3D Points!" );

  int NumRows = mxGetM ( prhs[P] );
  int NumElements = mxGetN ( prhs[P] );

  if ( ( NumRows != 3 && NumRows != 4 ) || NumElements < 1 )
    mexErrMsgTxt ( ERROR_HEADER "P must be a 3xn or 4xn (Homogeneous) matrix containing 3D Points!" );

  bool IsHomo = NumRows == 4;
  
#ifdef DO_DEBUG
  printf ( "Number of Points   = %d\n", NumElements );
  printf ( "Homogeneous Points = %s\n", ( IsHomo ? "true" : "false" ) );
#endif

  // Create Output
  plhs[R] = mxCreateDoubleMatrix ( 4, NumElements, mxREAL );
  double *r = (double *)mxGetPr ( plhs[R] );

  // Obtain Input Homogeneous Matrix
  double m11, m12, m13, m14, m21, m22, m23, m24, m31, m32, m33, m34, m41, m42, m43, m44;

  double *m = (double *)mxGetPr ( prhs[M] );
  m11 = *m++;  m21 = *m++;  m31 = *m++;  m41 = *m++;
  m12 = *m++;  m22 = *m++;  m32 = *m++;  m42 = *m++;
  m13 = *m++;  m23 = *m++;  m33 = *m++;  m43 = *m++;
  m14 = *m++;  m24 = *m++;  m34 = *m++;  m44 = *m++;

  // Obtain Input Array of Points
  double *p = (double *)mxGetPr ( prhs[P] );

  double px, py, pz, ps;
  double rx, ry, rz, rs;

  unsigned int i;
  for ( i = 0; i < NumElements; i++ )
  {
    px = *p++;  py = *p++;  pz = *p++;
    ps = IsHomo ? *p++ : 1.0;

    rx = m11 * px + m12 * py + m13 * pz + m14 * ps;
    ry = m21 * px + m22 * py + m23 * pz + m24 * ps;
    rz = m31 * px + m32 * py + m33 * pz + m34 * ps;

    // Should be one since last row of M is [0 0 0 1]:
    // *r++ = rx;  *r++ = ry;  *r++ = rz;  *r++ = 1.0;
    // But for compatibility with ApplyHomoToPosition.m is done like that:
    rs = m41 * px + m42 * py + m43 * pz + m44 * ps;
    *r++ = rx / rs;  *r++ = ry / rs;  *r++ = rz / rs;  *r++ = rs / rs;
  }
}
