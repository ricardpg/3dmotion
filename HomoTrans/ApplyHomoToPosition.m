%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Transform a set of 3D positions using an 4x4 homogeneous transformation.
%
%  File          : ApplyHomoToPosition.m
%  Date          : 02/02/2007 - 04/06/2008
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ApplyHomoToPosition Transform a set of 3D positions provided in homogeneous
%                      coordinates or not according to the given 4x4 matrix in
%                      homogeneous coordinates.
%
%      PointsR = ApplyHomoToPosition ( M, Points )
%
%     Input Parameters:
%      M: 4x4 Homogeneous 3D transformation.
%      Points: 4xn or 3xn set of 3D points. In the case that 4xN points,
%              the fourth row will be used to divide all other components.
%
%     Output Parameters:
%      PointsR: Resulting 3xK.
%

function PointsR = ApplyHomoToPosition ( M, Points )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Test Homogeneous Matrix
  [r, c] = size ( M );
  if ~isnumeric ( M ) || r ~= 4 || c ~= 4;
    error ( 'MATLAB:ApplyHomoToPosition:Input', ...
            'M (%dx%d) must be a 4x4 homogeneous transformation matrix!', r, c );
  end

  % Test the Input Points
  [m, n] = size ( Points );
  if ~isnumeric ( Points ) || ( m ~= 3 && m ~= 4 ) || n <= 0;
    error ( 'MATLAB:ApplyHomoToPosition:Input', ...
            'Points (%dx%d) must be a 3xn or 4xn set of 3D points!', m, n );
  end

  % Transform Input points to homogeneous coordinates
  P = ones ( 4, n );
  if m == 3;
    P(1:3, :) = Points;
  else
    P(1, :) = Points(1, :) ./ Points(4, :);
    P(2, :) = Points(2, :) ./ Points(4, :);
    P(3, :) = Points(3, :) ./ Points(4, :);
  end

  % Transform
  PointsR = M * P;

  % Convert output points to homogeneous coordinates
  PointsR(1, :) = PointsR(1, :) ./ PointsR(4, :);
  PointsR(2, :) = PointsR(2, :) ./ PointsR(4, :);
  PointsR(3, :) = PointsR(3, :) ./ PointsR(4, :);
  PointsR(4, :) = ones ( 1, n );
end
