%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute the inverse of a Pose represented in a Homogeneous Matrix.
%
%  File          : InvHomoPose.m
%  Date          : 29/11/2006 - 04/06/2008
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  InvHomoPose Compute the inverse of a 3D transformation matrix expressed
%              in homogeneous coordinates.
%
%      biMai = InvHomoPose ( aiMbi )
%
%     Input Parameters:
%      aiMbi: A 4x4xn set of matrices expressing poses in homogeneous
%             coordinates.
%
%     Input Parameters:
%      biMai: A 4x4xn matrix corresponding to the inverse of the input
%             aiMbi set of poses in homogeneous coordinates.
%

function biMai = InvHomoPose ( aiMbi )
  % Test the input parameters
  error ( nargchk ( 1, 1, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Set of input homogeneous matrices
  n = size ( aiMbi, 3 );

  % Preallocate
  biMai = zeros ( 4, 4, n );
  % Compute the inverse for all the input cameras
  for i = 1 : n;
    % Compute the inverse of a 3D homogenous transformation matrix
    % to avoid inv() function
    Rt = aiMbi(1:3,1:3,i)';
    biMai(1:3,1:3,i) = Rt;
    biMai(1:3,4,i) = -Rt * aiMbi(1:3,4,i);
    biMai(4,1:4,i) = [0 0 0 1];
  end
end
