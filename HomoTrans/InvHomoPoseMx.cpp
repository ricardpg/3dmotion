/*
  Autor(s)      : Jordi Ferrer Plana
  e-mail        : jferrerp@eia.udg.edu
  Branch        : Computer Vision

  Working Group : Underwater Vision Lab
  Project       : -

  Homepage      : http://porcsenglar.udg.edu

  Module        : Absolute Orientation problem using quaternions.

  File          : InvHomoPoseMx.cpp
  Date          : 08/05/2009 - 08/05/2009

  Compiler      : g++ >= 4.x
  Libraries     : -

  Notes         : - File written using ISO-8859-1 encoding.

 -----------------------------------------------------------------------------

  Copyright (C) 2009 by Jordi Ferrer Plana

  This source code is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This source code is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
  more details.

 -----------------------------------------------------------------------------
*/

#include <cmath>            // STL
#include <iostream>
#include <mex.h>            // Matlab Mex Functions

// Define for strict checking of the input/output parameters (slower)
// #define DO_STRICT_CHECK
// To ouptut Debug Information
// #define DO_DEBUG

#define ERROR_HEADER  "MATLAB:InvHomoPoseMx:Input\n"

// Identify the input parameters by it's index
enum { AIMBI = 0 };
// Identify the output parameters by it's index
enum { BIMAI = 0 };

// Mex function
//
void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter count checks
#ifdef DO_STRICT_CHECK
  if ( ( nrhs != 1 ) || ( nlhs != 1 ) )
#else
  if ( nrhs != 1 )
#endif
    mexErrMsgTxt ( ERROR_HEADER "Usage:  biMai = InvHomoPoseMx ( aiMbi )" );

  if ( mxGetNumberOfDimensions ( prhs[AIMBI] ) < 2 || !mxIsDouble ( prhs[AIMBI] ) )
    mexErrMsgTxt ( ERROR_HEADER "aiMbi must be have 2 or more dimensions!" );

  const mwSize *Sizes = mxGetDimensions ( prhs[AIMBI] );

  int NumRows = Sizes[0];
  int NumCols = Sizes[1];

#ifdef DO_DEBUG
  for ( int j = 0; j < mxGetNumberOfDimensions ( prhs[AIMBI] ); j++ )
    printf ( "Size of dimension %d = %d\n", j + 1, Sizes[j] );

  printf ( "Number of Rows = %d\n", NumRows );
  printf ( "Number of Cols = %d\n", NumCols );
#endif

  if ( NumRows != NumCols || NumRows != 4 )
    mexErrMsgTxt ( ERROR_HEADER "aiMbi must be a 4x4xn set of Homogenous Matrices!" );

  int NumElements;
  
  // Create Numeric Output
  if ( mxGetNumberOfDimensions ( prhs[AIMBI] ) == 2 )
  {
    NumElements = 1;
    plhs[BIMAI] = mxCreateDoubleMatrix ( 4, 4, mxREAL );
  }
  else
  {
    NumElements = Sizes[2];
    plhs[BIMAI] = mxCreateNumericArray ( 3, Sizes, mxDOUBLE_CLASS, mxREAL );
  }

#ifdef DO_DEBUG
  printf ( "Number of Elements = %d\n", NumElements );
#endif

  // Obtain Input Array
  double *aiMbi = (double *)mxGetPr ( prhs[AIMBI] );

  // Obtain Ouptut Array
  double *biMai = (double *)mxGetPr ( plhs[BIMAI] );

  double r[9];
  double t[3];

  unsigned int i;
  for ( i = 0; i < NumElements; i++ )
  {
    // Read Homogeneous Matrix
    r[0] = *aiMbi++;   r[1] = *aiMbi++;   r[2] = *aiMbi++;   aiMbi++;
    r[3] = *aiMbi++;   r[4] = *aiMbi++;   r[5] = *aiMbi++;   aiMbi++;
    r[6] = *aiMbi++;   r[7] = *aiMbi++;   r[8] = *aiMbi++;   aiMbi++;
    t[0] = *aiMbi++;   t[1] = *aiMbi++;   t[2] = *aiMbi++;   *aiMbi++ = 1.0;

    // Invert it
    *biMai++ = r[0];   *biMai++ = r[3];   *biMai++ = r[6];  *biMai++ = 0.0;
    *biMai++ = r[1];   *biMai++ = r[4];   *biMai++ = r[7];  *biMai++ = 0.0;
    *biMai++ = r[2];   *biMai++ = r[5];   *biMai++ = r[8];  *biMai++ = 0.0;
    *biMai++ = -r[0] * t[0] - r[1] * t[1] - r[2] * t[2];
    *biMai++ = -r[3] * t[0] - r[4] * t[1] - r[5] * t[2];
    *biMai++ = -r[6] * t[0] - r[7] * t[1] - r[8] * t[2];
    *biMai++ = 1.0;
  }
}
