%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Convert a set of poses in homogeneous coordinates to a quaternion
%                  and a translation vector (qr, q1, q2, q3, X, Y, Z ).
%
%  File          : HomoToPose.m
%  Date          : 30/01/2007 - 06/06/2008
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  HomoToPose Convert a sequence of N Homogeneous 4x4 pose matrices
%             to a set of 7xN column vectors containing a quaternion
%             and a translation (qr, q1, q2, q3, X, Y, Z) corresponding
%             to each camera pose.
%
%      Poses = HomoToPose ( Rs, ts )
%
%     Input Parameters:
%      H: Set of 4x4xN Homogeneous matrices containing the camera poses.
%
%     Output Parameters:
%      Poses: 7xN matrix containing the camera poses in the format:
%             (qr, q1, q2, q3, X, Y, Z).
%

function Poses = HomoToPose ( H )
  % Test the input parameters
  error ( nargchk ( 1, 1, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Test the input size
  [Rr, Rc, N] = size ( H );
  if Rr ~= 4 || Rc ~= 4 || N <= 0;
    error ( 'MATLAB:HomoToPose:Input', 'H must be a 4x4xN set of matrices!' );
  end

  Poses = zeros ( 7, N );
  for i = 1 : N;
    if isequal ( H(4, :, i), [ 0 0 0 1] );
      Poses(1:4, i) = rotmat2quat ( H(1:3, 1:3, i) )';
      Poses(5:7, i ) = H(1:3, 4, i);
    else
      error ( 'MATLAB:HomoToPose:Input', ...
              'Each Homogeneus matrix must have [0 0 0 1] as 4th row!' );
    end
  end
end
