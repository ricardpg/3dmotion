%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Transform a set of poses by a 4x4 homogeneous transformation.
%
%  File          : ApplyHomoToHomo.m
%  Date          : 10/05/2008 - 04/06/2008
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ApplyHomoToHomo Transform a set of poses provided in homogeneous
%                  coordinates according to the given 4x4 transformation
%                  matrix in homogeneous coordinates.
%                  If the first parameter (HorPoses) is a single transformation (4x4x1)
%                  and the second (PosesorH) is a (4x4xk, k >= 1) set of poses, then:
%
%                       PosesR = HorPoses * PosesorH
%
%                  If the second parameter (PosesorH) is a single transformation (4x4x1)
%                  and the first (HorPoses) is a (4x4xk, k > 1) set of poses, then:
%
%                       PosesR = PosesorH * HorPoses
%
%      PosesR = ApplyHomoToHomo ( HorPoses, PosesorH )
%
%     Input Parameters:
%      HorPoses: 4x4xk Homogeneous 3D transformations.
%      PosesorH: 4x4xk Homogeneous 3D transformations.
%
%     Output Parameters:
%      PosesR: Result of applying the transformation according to the
%              definition above.
%

function PosesR = ApplyHomoToHomo ( HorPoses, PosesorH )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Test the input Poses
  [r1, c1, k1] = size ( HorPoses );
  if ~isnumeric ( HorPoses ) || r1 ~= 4 || c1 ~= 4 || k1 <= 0;
    error ( 'MATLAB:ApplyHomoToHomo:Input', ...
            'HorPoses (%dx%dx%d) must be a 4x4xk, k > 0 set of poses in  homogeneous coordinates!', ...
            r1, c1, k1 );
  end

  % Test the Input Poses
  [r2, c2, k2] = size ( PosesorH );
  if ~isnumeric ( PosesorH ) || r2 ~= 4 || c2 ~= 4 || k2 <= 0;
    error ( 'MATLAB:ApplyHomoToHomo:Input', ...
            'PosesorH (%dx%dx%d) must be a 4x4xK, k > 0 set of poses in homogeneous coordinates!', ...
            r2, c2, k2 );
  end

  if ~( k1 == 1 || k2 == 1 );
    error ( 'MATLAB:ApplyHomoToHomo:Input', ...
            'Either k1 (%d) or k2 (%d), i.e. the size of the input parameters must be a single homoegeneous transformation!', ...
            k1, k2 );
  end
    
  % Look for where is the Single Transformation
  if k1 > k2;
    k = k1;
  else
    k = k2;
  end

  % Allocate for the Result
  PosesR = zeros ( 4, 4, k );

  % Check the Type of Transformation
  if k1 > k2;
    % Transform Set * H
    for i = 1 : k;
      PosesR(:,:,i) = HorPoses(:,:,i) * PosesorH;
    end
  else
    % Transform H * Set
    for i = 1 : k;
      PosesR(:,:,i) = PosesorH * HorPoses(:,:,i);
    end
  end
end
