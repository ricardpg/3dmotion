%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Rotation Matrix around the Z axis.
%
%  File          : RotZ.m
%  Date          : 17/07/2007 - 04/06/2008
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2008 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  RotZ Calculate the rotation matrix in Z for a given angle in radiants.
%
%      R = RotZ ( a )
%
%     Input Parameters:
%      a: Angle in radiants.
%
%     Output Parameters:
%      R: 3x3 rotation matrix corresponding to a rotation of a radiants in
%         the Z axis.
%

function R = RotZ ( a )
  % Compute a Rotation Matrix around Z Axis of a Rads
  R = [  cos(a), -sin(a),       0;
         sin(a),  cos(a),       0;
              0,       0,       1 ];
end
