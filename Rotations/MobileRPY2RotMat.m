%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Convert Angles Roll, Pitch and Yaw to a Rotation Matrix R.
%
%  File          : MobileRPY2RotMat.m
%  Date          : 05/11/2007 - 06/11/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  MobileRPY2RotMat Composition of 3 Rotation Angles R, P and Y to obtain a
%                   3x3 Rotation Matrix.
%                   The used convention is Mobile Roll, Pitch and Yaw used in nautics:
%                     Rxyz(Roll, Pitch, Yaw) = Rx(Roll) * Ry(Pitch) * Rz(Yaw)
%                   So, first there is a Roll rotation wrt. the X axis, then a Beta
%                   rotation wrt. the Y axis and, finally, a Yaw rotation wrt. the Z axis
%                   allways defined in the MOBILE axis reference frame.
%
%     Input Parameters:
%      Roll, Pitch, Yaw: 3D Rigid Motion angles.
%
%     Output Parameters:
%      R: 3x3 Rotation Matrix.
%

function R = MobileRPY2RotMat ( Roll, Pitch, Yaw )

%  >> R = sym ( 'R', 'real' );
%  >> P = sym ( 'P', 'real' );
%  >> Y = sym ( 'Y', 'real' );
%  >> Rx = [1 0 0 ; 0 cos(R) -sin(R); 0 sin(R) cos(R)];
%  >> Ry = [ cos(P) 0 sin(P); 0 1 0 ; -sin(P) 0 cos(P)];
%  >> Rz = [ cos(Y) -sin(Y) 0 ; sin(Y) cos(Y) 0; 0 0 1];
%  >> Rxyz = Rx * Ry * Rz
%
%
%   Rxyz =
%     [                       cos(P)*cos(Y),                      -cos(P)*sin(Y),                              sin(P)]
%     [  sin(R)*sin(P)*cos(Y)+cos(R)*sin(Y), -sin(R)*sin(P)*sin(Y)+cos(R)*cos(Y),                      -sin(R)*cos(P)]
%     [ -cos(R)*sin(P)*cos(Y)+sin(R)*sin(Y),  cos(R)*sin(P)*sin(Y)+sin(R)*cos(Y),                       cos(R)*cos(P)]

  R = RotX ( Roll ) * RotY ( Pitch ) * RotZ ( Yaw );
end
