%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Normalize angles into [-pi, pi] range.
%
%  File          : NormAngle.m
%  Date          : 25/05/2009 - 25/05/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  NormAngle Normalize input angles into the range [-pi, pi] range.
%
%      A = NormAngle ( A0 )
%
%     Input Parameters:
%      A0: Input 1xn vector containing angles in radiants.
%
%     Output Parameters:
%      A: Output 1xn vector with angles normalized in the range [-pi, pi].
%

function A = NormAngle ( A0 )
  % Test the input parameters
  error ( nargchk ( 1, 1, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  [N M] = size ( A0 );
  if N ~= 1 || M < 1;
    error ( 'MATLAB:NormAngle:Check', 'Input A0 must be a 1xn vector containing angles in radiants!' );
  end
  
  A = zeros ( 1, M );
  for i = 1 : M;
    % Subtract complete turns (2*pi rad)
    An = mod ( A0(i), 2 * pi );
%    An = A0(i);
%    while abs ( An ) > 2 * pi;
%      An = An - sign ( An ) * 2 * pi;
%    end
    % Normalize into [-pi, pi] range
    if An > pi;  An = An - 2.0 * pi; end;
    if An < -pi; An = An + 2.0 * pi; end;

    % Assign to the vector
    A(i) = An;
  end
end
