%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Convert a unit Quaternion q to Angles Roll, Pitch and Yaw.
%
%  File          : Quat2MobileRPY.m
%  Date          : 11/03/2009 - 12/03/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - See RotMat2FixedXYZ to use other conventions.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  Quat2MobileRPY Decomposition of a 4x1 Unit Quaternion to the 3 Angles that are used
%                 to obtain the Rotation Matrix.
%                 The used convention is Mobile Roll, Pitch and Yaw used in nautics:
%                     Rxyz(Roll, Pitch, Yaw) = Rx(Roll) * Ry(Pitch) * Rz(Yaw)
%                 So, first there is a Roll rotation wrt. the X axis, then a Beta
%                 rotation wrt. the Y axis and, finally, a Yaw rotation wrt. the Z axis
%                 allways defined in the MOBILE axis reference frame.
%
%     Input Parameters:
%      R: 4x1 Quaternion.
%
%     Output Parameters:
%      Roll, Pitch, Yaw: 3D Rigid Motion angles.
%

function [Roll, Pitch, Yaw] = Quat2MobileRPY ( R )

%  >> R = sym ( 'R', 'real' );
%  >> P = sym ( 'P', 'real' );
%  >> Y = sym ( 'Y', 'real' );
%  >> Rx = [1 0 0 ; 0 cos(R) -sin(R); 0 sin(R) cos(R)];
%  >> Ry = [ cos(P) 0 sin(P); 0 1 0 ; -sin(P) 0 cos(P)];
%  >> Rz = [ cos(Y) -sin(Y) 0 ; sin(Y) cos(Y) 0; 0 0 1];
%  >> Rxyz = Rx * Ry * Rz
%
%   Rxyz =
%     [                       cos(P)*cos(Y),                      -cos(P)*sin(Y),                              sin(P)]
%     [  sin(R)*sin(P)*cos(Y)+cos(R)*sin(Y), -sin(R)*sin(P)*sin(Y)+cos(R)*cos(Y),                      -sin(R)*cos(P)]
%     [ -cos(R)*sin(P)*cos(Y)+sin(R)*sin(Y),  cos(R)*sin(P)*sin(Y)+sin(R)*cos(Y),                       cos(R)*cos(P)]
%
%  >> Rqx = cos(R/2) + i * sin(R/2)
%  >> Rqy = cos(P/2) + j * sin(P/2)
%  >> Rqz = cos(Y/2) + k * sin(Y/2)
%
%  >> Rqx = [ cos(R/2), sin(R/2), 0, 0 ];
%  >> Rqy = [ cos(P/2), 0, sin(P/2), 0 ];
%  >> Rqz = [ cos(Y/2), 0, 0, sin(Y/2) ];
%  >> Rqxyz = quatprod ( quatprod ( qx, qy ), qz )'
%
%   Rqxyz = q
%     cos(P/2)*cos(R/2)*cos(Y/2) - sin(P/2)*sin(R/2)*sin(Y/2)
%     cos(P/2)*cos(Y/2)*sin(R/2) + cos(R/2)*sin(P/2)*sin(Y/2)
%     cos(R/2)*cos(Y/2)*sin(P/2) - cos(P/2)*sin(R/2)*sin(Y/2)
%     cos(P/2)*cos(R/2)*sin(Y/2) + cos(Y/2)*sin(P/2)*sin(R/2)
%
%   Rqxyz = q
%     cos(R/2)*cos(P/2)*cos(Y/2) - sin(R/2)*sin(P/2)*sin(Y/2)
%     sin(R/2)*cos(P/2)*cos(Y/2) + cos(R/2)*sin(P/2)*sin(Y/2)
%     cos(R/2)*sin(P/2)*cos(Y/2) - sin(R/2)*cos(P/2)*sin(Y/2)
%     cos(R/2)*cos(P/2)*sin(Y/2) + sin(R/2)*sin(P/2)*cos(Y/2)

  error ( 'MATLAB:Quat2MobileRPY:Check', 'Function still TODO (see Quat2FixedXYZ.m)!' );
end
