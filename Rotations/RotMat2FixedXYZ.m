%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Convert Rotation Matrix R to Angles Gamma, Beta and Alpha.
%
%  File          : RotMat2FixedXYZ.m
%  Date          : 17/07/2007 - 04/06/2008
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - See RotMat2MobileRPY to use other conventions.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  RotMat2FixedXYZ Decomposition of a 3x3 Rotation Matrix to the 3 Angles that are used
%                  to obtain the Rotation Matrix.
%                  The used convention is Fixed X-Y-Z in Craig (pg 47):
%                       Rxyz(G, B, A) = Rz(A) * Ry(B) * Rx(G)
%                  So, first there is a Gamma rotation wrt. the X axis, then a Beta
%                  rotation wrt. the Y axis and, finally, an Alpha rotation wrt.
%                  the Z axis allways defined in the FIXED axis reference frame.
%
%                  This convention IS the convention in the Craig, pag. 47!
%
%     Input Parameters:
%      R: 3x3 Rotation Matrix.
%
%     Output Parameters:
%      G, B, A: 3D Rigid Motion angles.
%

function [G, B, A] = RotMat2FixedXYZ ( R )

%  >> G = sym ( 'G', 'real' );
%  >> B = sym ( 'B', 'real' );
%  >> A = sym ( 'A', 'real' );
%  >> Rx = [1 0 0 ; 0 cos(G) -sin(G); 0 sin(G) cos(G)];
%  >> Ry = [ cos(B) 0 sin(B); 0 1 0 ; -sin(B) 0 cos(B)];
%  >> Rz = [ cos(A) -sin(A) 0 ; sin(A) cos(A) 0; 0 0 1];
%  >> Rxyz = Rz * Ry * Rx
%
%   Rxyz =
%     [                       cos(A)*cos(B), -sin(A)*cos(G)+cos(A)*sin(B)*sin(G),  sin(A)*sin(G)+cos(A)*sin(B)*cos(G)]
%     [                       sin(A)*cos(B),  cos(A)*cos(G)+sin(A)*sin(B)*sin(G), -cos(A)*sin(G)+sin(A)*sin(B)*cos(G)]
%     [                             -sin(B),                       cos(B)*sin(G),                       cos(B)*cos(G)]

  Px = sqrt ( R(3,2)*R(3,2) + R(3,3)*R(3,3) );

  % Check Singularity
  if Px > 1e-8;
    B = atan2 ( -R(3,1), Px );
%    cP = cos ( B );
%    A = atan2 ( R(2,1)/cP, R(1,1)/cP );
%    G = atan2 ( R(3,2)/cP, R(3,3)/cP );

     % Avoid dividing and calculating a cosine, just use the cos sign instead
     if ( B < pi/2 ) && ( B > -pi/2 );
       A = atan2 ( R(2,1), R(1,1) );
       G = atan2 ( R(3,2), R(3,3) );
     else
       A = atan2 ( -R(2,1), -R(1,1) );
       G = atan2 ( -R(3,2), -R(3,3) );
     end
  else
    B = pi / 2;
    % cos ( B ) = 0;  Sin ( B ) = 1
    % Only the sum A + G can be computed => Convention A = 0
    % cos ( A ) = 1; sin ( A ) = 0
    A = 0;
    G = atan2 ( -R(2,3), R(2,2) );
  end
end
