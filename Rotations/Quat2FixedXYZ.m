%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Convert a unit Quaternion q to Angles Gamma, Beta and Alpha.
%
%  File          : Quat2FixedXYZ.m
%  Date          : 11/03/2009 - 12/03/2009
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - See RotMat2MobileRPY to use other conventions.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2009 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  Quat2FixedXYZ Decomposition of a 4x1 Unit Quaternion to the 3 Angles that are used
%                to obtain the quaternion.
%                The used convention is Fixed X-Y-Z in Craig (pg 47):
%                     Rxyz(G, B, A) = Rz(A) * Ry(B) * Rx(G)
%                So, first there is a Gamma rotation wrt. the X axis, then a Beta
%                rotation wrt. the Y axis and, finally, an Alpha rotation wrt.
%                the Z axis allways defined in the FIXED axis reference frame.
%
%                This convention IS the convention in the Craig, pag. 47!
%
%     Input Parameters:
%      R: 4x1 Quaternion.
%
%     Output Parameters:
%      G, B, A: 3D Rigid Motion angles.
%

function [G, B, A] = Quat2FixedXYZ ( q )

%  >> G = sym ( 'G', 'real' );
%  >> B = sym ( 'B', 'real' );
%  >> A = sym ( 'A', 'real' );
%  >> Rx = [1 0 0 ; 0 cos(G) -sin(G); 0 sin(G) cos(G)];
%  >> Ry = [ cos(B) 0 sin(B); 0 1 0 ; -sin(B) 0 cos(B)];
%  >> Rz = [ cos(A) -sin(A) 0 ; sin(A) cos(A) 0; 0 0 1];
%  >> Rxyz = Rz * Ry * Rx
%
%   Rxyz =
%     [                       cos(A)*cos(B), -sin(A)*cos(G)+cos(A)*sin(B)*sin(G),  sin(A)*sin(G)+cos(A)*sin(B)*cos(G)]
%     [                       sin(A)*cos(B),  cos(A)*cos(G)+sin(A)*sin(B)*sin(G), -cos(A)*sin(G)+sin(A)*sin(B)*cos(G)]
%     [                             -sin(B),                       cos(B)*sin(G),                       cos(B)*cos(G)]
%
%  >> qx = cos(G/2) + i * sin(G/2)
%  >> qy = cos(B/2) + j * sin(B/2)
%  >> qz = cos(A/2) + k * sin(A/2)
%
%  >> qx = [ cos(G/2), sin(G/2), 0, 0 ];
%  >> qy = [ cos(B/2), 0, sin(B/2), 0 ];
%  >> qz = [ cos(A/2), 0, 0, sin(A/2) ];
%  >> qxyz = quatprod ( quatprod ( qz, qy ), qx )'
%
%   qxyz = q =
%     cos(A/2)*cos(B/2)*cos(G/2) + sin(A/2)*sin(B/2)*sin(G/2)
%     cos(A/2)*cos(B/2)*sin(G/2) - cos(G/2)*sin(A/2)*sin(B/2)
%     cos(A/2)*cos(G/2)*sin(B/2) + cos(B/2)*sin(A/2)*sin(G/2)
%     cos(B/2)*cos(G/2)*sin(A/2) - cos(A/2)*sin(B/2)*sin(G/2)
%
%   qxyz = q =
%     cos(G/2)*cos(B/2)*cos(A/2) + sin(G/2)*sin(B/2)*sin(A/2)
%     sin(G/2)*cos(B/2)*cos(A/2) - cos(G/2)*sin(B/2)*sin(A/2)
%     cos(G/2)*sin(B/2)*cos(A/2) + sin(G/2)*cos(B/2)*sin(A/2)
%     cos(G/2)*cos(B/2)*sin(A/2) - sin(G/2)*sin(B/2)*cos(A/2)
%
%
%   Quaternion to rotation matrix (Salamin '79):
%
%         |  q02+qx2-qy2-qz2         -q0z+qxy          q0y+qxz |
%     R = |          q0z+qxy  q02-qx2+qy2-qz2         -q0x+qyz |
%         |         -q0y+qxz          q0x+qyz  q02-qx2-qy2+qz2 |
%
%  >> Rqxyz = [ q(1)*q(1)+q(2)*q(2)-q(3)*q(3)-q(4)*q(4),          -q(1)*q(4)+q(2)*q(3),                         q(1)*q(3)+q(2)*q(4);
%                       q(1)*q(4)+q(2)*q(3),              q(1)*q(1)-q(2)*q(2)+q(3)*q(3)-q(4)*q(4),             -q(1)*q(2)+q(3)*q(4);
%                      -q(1)*q(3)+q(2)*q(4),                      q(1)*q(2)+q(3)*q(4),                q(1)*q(1)-q(2)*q(2)-q(3)*q(3)+q(4)*q(4) ]
%
%
%   Quaternion to rotation matrix (Id Software, http://cache-www.intel.com/cd/00/00/29/37/293748_293748.pdf)
%
%          | 1 - 2qy^2 - 2qz^2     2qxy + 2q0z        2qxz - 2q0y    |
%     R' = |    2qxy - 2q0z     1 - 2qx^2 - 2qz^2     2qyz + 2q0x    |
%          |    2qxz + 2q0y        2qyz - 2q0x     1 - 2qx^2 - 2qy^2 | 
%
%              
%  >> Rqxyz' = [ 1-2*q(3)*q(3)-2*q(4)*q(4),  2*q(2)*q(3)+2*q(1)*q(4),    2*q(2)*q(4)-2*q(1)*q(3);
%                 2*q(2)*q(3)-2*q(1)*q(4),  1-2*q(2)*q(2)-2*q(4)*q(4),   2*q(3)*q(4)+2*q(1)*q(2);
%                 2*q(2)*q(4)+2*q(1)*q(3),   2*q(3)*q(4)-2*q(1)*q(2),   1-2*q(2)*q(2)-2*q(3)*q(3) ];
%
%  >> Rqxyz  = [ 1-2*q(3)*q(3)-2*q(4)*q(4),  2*q(2)*q(3)-2*q(1)*q(4),    2*q(2)*q(4)+2*q(1)*q(3);
%                 2*q(2)*q(3)+2*q(1)*q(4),  1-2*q(2)*q(2)-2*q(4)*q(4),   2*q(3)*q(4)-2*q(1)*q(2);
%                 2*q(2)*q(4)-2*q(1)*q(3),   2*q(3)*q(4)+2*q(1)*q(2),   1-2*q(2)*q(2)-2*q(3)*q(3) ];
%
%
%   Following convertion (http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles) has de singularity
%
%     G = arctan ( ( 2 * ( q0*q1 + q2*q3 ) ) / ( 1 - 2 * ( q1^2 + q2^2 ) ) )
%     B = asin ( 2 * ( q0*q2 - q3 * q1 ) )
%     A = arctan ( ( 2 * ( q0*q3 + q1*q2 ) ) / ( 1 - 2 * ( q2^2 + q3^2 ) ) )
% 
%     G = atan ( ( 2 * ( q(1)*q(2) + q(3)*q(4) ) ) / ( 1 - 2 * ( q(2)*q(2) + q(3)*q(3) ) ) );
%     B = asin ( 2 * ( q(1)*q(3) - q(4)*q(2) ) );
%     A = atan ( ( 2 * ( q(1)*q(4) + q(2)*q(3) ) ) / ( 1 - 2 * ( q(3)*q(3) + q(4)*q(4) ) ) );
%

%
%   TODO. Check Singularity for obtaining the angles (Follow file RotMat2FixedXYZ.m algorithm):
%
%  >> q0 = sym ( 'q0', 'real' );
%  >> qx = sym ( 'qx', 'real' );
%  >> qy = sym ( 'qy', 'real' );
%  >> qz = sym ( 'qz', 'real' );
%
%  >> Rqxyz  = [ 1-2*qy*qy-2*qz*qz,  2*qx*qy-2*q0*qz,    2*qx*qz+2*q0*qy;
%                 2*qx*qy+2*q0*qz,  1-2*qx*qx-2*qz*qz,   2*qy*qz-2*q0*qx;
%                 2*qx*qz-2*q0*qy,   2*qy*qz+2*q0*qx,   1-2*qx*qx-2*qy*qy ];
%
%  
%
%   Px = sqrt ( Rqxyz(3,2)*Rqxyz(3,2) + Rqxyz(3,3)*Rqxyz(3,3) );
%   Px = (2*qx^2 + 2*qy^2 - 1)^2 + (2*q0*qx + 2*qy*qz)^2
%
%   % Check Singularity
%   if Px > 1e-8;
%     B = atan2 ( -R(3,1), Px );
%
%    ...
%
  error ( 'MATLAB:Quat2MobileRPY:Check', 'Function still TODO!' );
end
  